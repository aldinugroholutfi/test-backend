/**
 *  Retrieves a single document by id.
 *  @param {string} req.params.id
 *  @returns {Document} Single Document
 */
const bcrypt = require("bcryptjs");
const fs = require("fs");
const {PrismaClient} = require("@prisma/client");
const {Model} = require("mongoose");
const prisma = new PrismaClient();
const axios = require('axios');

exports.create = async (req, res) => {
    try {
        // Extract user data from the request
        const {firstName, lastName, birthday, location} = req.body;
        const email = firstName + '' + lastName + '@gmail.com';
        // Create the user in the database
        const result = await prisma.user.create({
            data: {
                email, firstName, lastName, birthday, location,
            },
        });

        const user = {
            email: result.email,
            birthday: result.birthday,
            firstName: result.firstName,
            lastName: result.lastName,
            location: result.location
        }

        // Send a success response with the created user
        res.status(201).json({success: true, message: "User created successfully", user});
    } catch (error) {
        // Handle any errors that occur during the creation process
        console.error("Error creating user:", error);
        res.status(500).json({success: false, message: "Error creating user", error});
    }
};

exports.delete = async (req, res) => {
    // Get the user ID from the request parameters
    const {userId} = req.body;
    // console.log("Test : ",userId);

    try {
        // Use Prisma to delete the user by ID
        const deletedUser = await prisma.user.delete({
            where: {
                id: userId, // Assuming userId is a numeric ID
            },
        });

        if (!deletedUser) {
            return res.status(404).json({success: false, message: 'User not found'});
        }

        return res.status(200).json({success: true, message: 'User deleted successfully'});
    } catch (error) {
        console.error('Error deleting user:', error);
        return res.status(500).json({success: false, message: 'An error occurred while deleting the user'});
    }
};

exports.sendBirthdayMessage = async (req, res) => {
    try {
        // Get the current date and time
        const currentDate = new Date();

        // Fetch all users from the database
        const users = await prisma.user.findMany();

        // Initialize a variable to track if any messages were successfully sent
        let messagesSent = false;

        // Retry settings
        const maxRetries = 3; // Maximum number of retries
        const retryDelay = 5000; // Delay between retries in milliseconds (5 seconds)

        // Loop through the users and check if it's their birthday today
        for (const user of users) {
            const userBirthday = new Date(user.birthday);

            // Check if today is the user's birthday
            if (userBirthday.getDate() === currentDate.getDate() && userBirthday.getMonth() === currentDate.getMonth()) {
                // Calculate the user's local 9:00 AM time
                const userLocalNineAM = new Date(userBirthday.getFullYear(), userBirthday.getMonth(), userBirthday.getDate(), 9, // 9:00 AM
                    0 // 0 minutes
                );

                // Adjust userLocalNineAM for the user's time zone
                const userLocalTimeZone = user.location;
                const userLocalNineAMInTimeZone = new Date(userLocalNineAM.toLocaleString('en-US', {
                    timeZone: userLocalTimeZone,
                }));

                // Check if the current time is equal to user's local 9:00 AM
                if (currentDate.getTime() === userLocalNineAMInTimeZone.getTime()) {
                    // Prepare the birthday message
                    const birthdayMessage = `Hey, ${user.firstName}, it's your birthday!`;

                    // Prepare the API request payload
                    const apiPayload = {
                        email: user.email, message: birthdayMessage,
                    };

                    let retries = 0;
                    let response;

                    // Lock mechanism to prevent race conditions
                    if (!user.isSendingBirthdayMessage) {
                        user.isSendingBirthdayMessage = true;

                        // Retry the API call in case of timeout or random errors
                        while (retries < maxRetries) {
                            try {
                                // Send the birthday message to the API
                                response = await axios.post('https://email-service.digitalenvision.com.au/send-email', apiPayload);

                                // Check if the response status is 200
                                if (response.status === 200) {
                                    messagesSent = true;
                                    break; // Exit the retry loop if successful
                                }
                            } catch (error) {
                                console.error('Error sending birthday message:', error);
                            }

                            // Wait for the specified retry delay before retrying
                            await new Promise(resolve => setTimeout(resolve, retryDelay));

                            retries++;
                        }

                        // Unlock the user to allow sending messages to other users
                        user.isSendingBirthdayMessage = false;

                        // Check if messages were successfully sent and break the loop
                        if (messagesSent) {
                            break;
                        }
                    }
                }
            }
        }

        // Check if no messages were sent
        if (!messagesSent) {
            console.log('No birthday messages were sent at 9:00 AM local time.');
        } else {
            console.log('Birthday messages sent successfully');
        }

        // Respond with a success message
        res.status(200).json({message: 'Birthday messages sent successfully'});
    } catch (error) {
        console.error('Error sending birthday messages:', error);

        // Respond with an error message
        res.status(500).json({error: 'Error sending birthday messages'});
    }

};

exports.update = async (req, res) => {
    try {
        const {userId} = req.params;
        const updatedUserData = req.body;

        // Update the user's details in the database
        const updatedUser = await prisma.user.update({
            where: {id: userId}, data: updatedUserData,
        });

        // Respond with the updated user data
        res.status(200).json(updatedUser);
    } catch (error) {
        console.error('Error updating user:', error);
        res.status(500).json({error: 'Error updating user'});
    }
};

