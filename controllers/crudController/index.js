const crudMethods = require("./crudMethods");
const mongoose = require("mongoose");

exports.crudController = (modelName) => {
    // const Model = mongoose.model(modelName);
    let methods = {};

    methods.create = async (req, res) => {
        await crudMethods.create(req, res);
    };

    methods.delete = async (req, res) => {
        await crudMethods.delete(req, res);
    };

    methods.update = async (req, res) => {
        await crudMethods.update(req, res);
    };

    methods.list = async (req, res) => {
        await crudMethods.list(req, res);
    };

    methods.sendBirthdayMessage = async (req, res) => {
        await crudMethods.sendBirthdayMessage(req, res);
    };

    return methods;
};