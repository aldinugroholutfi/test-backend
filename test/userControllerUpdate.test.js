const {PrismaClient} = require('@prisma/client');
const userController = require('../controllers/userController');
const {advanceTo, clear} = require('jest-date-mock');
const axios = require('axios');

// Mock PrismaClient
const prisma = new PrismaClient();

jest.mock('axios'); // Mock the entire axios module

describe('User Controller - updateUser', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    afterAll(() => {
        prisma.$disconnect();
        clear(); // Clear mocked date
    });

    it('should update user details and deliver birthday message on the correct day', async () => {
        // Mock the user data and update request
        const mockUser = {
            id: '64faea305fade90ca9629893',
            email: 'DavidWilliams@gmail.com',
            firstName: 'David',
            lastName: 'Williams',
            birthday: new Date('1990-09-09T00:00:00.000Z'),
            location: 'Asia/Jakarta',
            createdAt: new Date('2023-09-08T09:32:32.127Z')
        };
        const updatedUserData = {
            // Updated user data here (e.g., birthday)
            birthday: new Date('1990-09-09T00:00:00.000Z'),
        };

        // Mock the behavior of Prisma's `user.update`
        prisma.user.update = jest.fn().mockImplementation(async (params) => {
            // Check if the update params match the expected structure
            if (params.where.id === mockUser.id && params.data.birthday === updatedUserData.birthday) {

                return mockUser; // Return the updated user data
            } else {
                throw new Error('Invalid update parameters');
            }
        });

        // Set the current date and time to local 9:00 AM on the user's birthday
        advanceTo(new Date('1990-09-09T09:00:00'));

        const req = {
            params: {userId: mockUser.id}, body: updatedUserData,
        };
        const res = {
            status: jest.fn().mockReturnThis(), json: jest.fn(),
        };

        await userController.update(req, res);

        // Expect the response to indicate success
        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledWith(mockUser);

        // Ensure that Axios was called with the updated birthday message
        expect(axios.post).toHaveBeenCalledWith('https://email-service.digitalenvision.com.au/send-email', expect.objectContaining({
            email: mockUser.email, message: `Hey, ${mockUser.firstName}, it's your birthday!`,
        }));

        // Clear the mocked date to avoid interfering with other tests
        clear();
    });
});
