## BACKEND-TEST

I build based on stack (Express.js / MongoDb / Prisma)

## Setup backend

1) just create MongoDB Atlas accout database url 
2) change this file name .variables.env.tmp to  .env
   If there is no file name .variables.env.tmp, just create new file .env and Copy Paste code below:

   # Rename this file from .variables.env.tmp to .env
   DATABASE_URL="mongodb+srv://aldinugroho999:aldinugroho999@cluster0.7kl3ikv.mongodb.net/test"
   NODE_ENV=development
   PORT=8888
   SECRET=hackthoon
   KEY=idurar

3) open .env and paste your MongoDB url here :  DATABASE=your-mongodb-url
4) npm install

## start server
1) npm start

## App Features :

### Backend :

* The backend is built with Node.js, [Express.js Framework](https://expressjs.com/), [MongoDB](https://www.mongodb.com/), [Prisma](https://www.prisma.io/).

### API :

* API Routes generated inside /routes/api.js
  CRUD Api scheme: Controller --> crudController (CREATE, LIST, UPDATE, DELETE, SENDBIRTHDAYMESSAGE)

  Api Route --> http://localhost:8888/api/

## `sendBirthdayMessage` Controller

The `sendBirthdayMessage` controller is responsible for sending birthday messages to users at their local 9:00 AM time. It optimizes the code using concurrency to efficiently send API requests to the email service, implements a locking mechanism to prevent multiple instances of the same message from being sent simultaneously, and handles timeouts gracefully.

### Key Features:

1. **Concurrency:** The controller takes advantage of asynchronous programming and concurrency to send birthday messages to multiple users simultaneously. This improves the efficiency of message delivery.

2. **Locking Mechanism:** To prevent race conditions and ensure that a message is sent to each user only once, a locking mechanism is implemented. It checks a flag (`isSendingBirthdayMessage`) before sending a message and ensures that only one instance of the message is sent per user.

3. **Timeout Handling:** The controller handles API call timeouts gracefully. It retries API requests in case of timeouts or random errors, with a maximum number of retries and a delay between retries. This ensures that messages are eventually sent, even if the initial API request times out.

By implementing these features, the `sendBirthdayMessage` controller ensures efficient, reliable, and timely delivery of birthday messages to users.

For more details on the implementation, refer to the [controller code](#controller-code).


## Unit Tests

The backend application is thoroughly tested with unit tests to ensure its functionality and reliability. Here are the unit tests included:

### User Create

- **Description:** This test verifies the functionality of creating a new user. It ensures that user data can be successfully added to the database.

### User Delete

- **Description:** This test validates the user deletion process. It checks if a user can be removed from the database without errors.

### User Update

- **Description:** The user update test checks the ability to update user details. It ensures that user data can be modified correctly.

### User Controller - sendBirthdayMessage

- **Description:** This set of tests focuses on the `sendBirthdayMessage` controller, responsible for sending birthday messages to users. It includes the following scenarios:

    - **should send birthday messages:** Verifies that birthday messages are sent to users at the appropriate time.
    
    - **should handle API timeout and eventually succeed:** Ensures that the system gracefully handles API timeouts and retries to send messages successfully.
    
    - **should handle API random error and eventually succeed:** Tests the system's resilience against random API errors by retrying and succeeding.
    
    - **should prevent handling race conditions and duplicate messages:** Validates that the controller prevents race conditions and ensures that duplicate messages are not sent.

These unit tests help maintain the reliability and functionality of the backend application while providing confidence in its performance.
