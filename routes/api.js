const express = require("express");
const {catchErrors} = require("../handlers/errorHandlers");

const router = express.Router();

const userController = require("../controllers/userController");

router.route("/user/create").post(catchErrors(userController.create));
router.route("/user/update/:id").put(catchErrors(userController.update));
router.route("/user/delete/:id").delete(catchErrors(userController.delete));
router.route("/user/list").get(catchErrors(userController.list));
router.route("/user/sendBirthdayMessage").get(catchErrors(userController.sendBirthdayMessage));

module.exports = router;