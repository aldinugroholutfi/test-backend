const express = require("express");
const session = require("express-session");
const MongoStore = require("connect-mongo");
const path = require("path");
const bodyParser = require("body-parser");
const {PrismaClient} = require('@prisma/client');
const axios = require('axios');
const apiRouter = require("./routes/api");
const nodeSchedule = require('node-schedule');

require("dotenv").config({path: ".env"});
require('./setup/scheduler'); // Adjust the path as needed

const prisma = new PrismaClient();

const app = express();

app.use(express.static(path.join(__dirname, "public")));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(session({
    secret: process.env.SECRET,
    key: process.env.KEY,
    resave: false,
    saveUninitialized: false,
    store: MongoStore.create({mongoUrl: process.env.DATABASE_URL}),
}));

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Methods", "GET,PATCH,PUT,POST,DELETE");
    res.header("Access-Control-Expose-Headers", "Content-Length");
    res.header("Access-Control-Allow-Headers", "Accept, Authorization,x-auth-token, Content-Type, X-Requested-With, Range");
    if (req.method === "OPTIONS") {
        return res.sendStatus(200);
    } else {
        return next();
    }
});

app.use("/api", apiRouter);

app.get('/', (req, res) => {
    res.send(`
    <div>
      <h1>Server is running!</h1>
    </div>
  `);
});

module.exports = app;
