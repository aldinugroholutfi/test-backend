const schedule = require('node-schedule');
const { sendBirthdayMessages } = require('../controllers/userController'); // Adjust the path as needed

// Schedule the function to run at 9:00 AM every day
const job = schedule.scheduleJob('0 9 * * *', () => {
    console.log('Running sendBirthdayMessages at 9:00 AM');
    sendBirthdayMessages(); // Call the function to send birthday messages
});
